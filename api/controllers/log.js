const Patient = require("../models/Patient");
const Doctor = require("../models/Doctor");
const Log = require("../models/Log");

// Create
exports.create = async (req, res, next) => {
  const {
    doctorId,
    patientId,
    date,
    time,
    description,
    completed,
    billable,
    amount,
    paid,
  } = req.body;
  let _d = {
    doctorId,
    patientId,
    date,
    time,
    description,
    completed: completed == 'yes' ? true : false,
    billable: billable == 'yes' ? true : false,
    amount: parseInt(amount),
    paid: paid == 'yes' ? true : false,
  };
  try {
    const _doctor = await Doctor.findOne({id: doctorId})
    const _patient = await Patient.findOne({id: patientId})

    _d.doctorId = _doctor._id
    _d.patientId = _patient._id
    const _log = new Log(_d);
    log = _log.save();

    return res.status(201).json({
      message: "New Procedure entry Successful",
      log,
    });
    // )
  } catch (error) {
    // if (error) {
    return res.status(409).json({
      error: true,
      message: "Record entry Failed",
      err: error,
    });
    // }
  }
};

// Read

exports.find = async (req, res) => {
  try {
    const patients = await User.find({});
    console.log(patients);
    return res.status(200).json({ patients });
  } catch (error) {
    console.log(error);
    return res.status(403).json({ error });
  }
};

exports.profile = async (req, res) => {
  try {
    const patient = await User.findOne({ id: req.params.id });
    // console.log({patient});
    ret = { patient };
    if (patient) {
      console.log({ patientId: req.params.id });
      const appointments = await Appointment.find({ patientId: req.params.id });
      ret.appointments = appointments;
    }
    console.log(ret);
    return res.status(200).json(ret);
  } catch (error) {
    console.log(error);
    return res.status(403).json({ error });
  }
};

// Update

exports.update = async (req, res) => {
  // console.log('here', req.body);
  // console.log(keys);
  // return res.json({});
  try {
    const user = await User.findOneAndUpdate({ id: req.params.id }, req.body, {
      new: true,
    });
    return res.status(200).json({
      user,
    });
  } catch (e) {
    return res.status(400).json({
      message: "Server Error",
      e,
    });
  }
};
