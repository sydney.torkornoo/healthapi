const Appointment = require("../models/Appointment");
const Doctor = require("../models/Doctor");
const Patient = require("../models/Patient");

// Create
exports.create = async (req, res, next) => {
  const { doctorId, patientId, date, time, duration } = req.body;
  let _d = {
    doctorId,
    patientId,
    date,
    time,
    duration,
  };
  // console.log('here')
  try {
    const _doctor = await Doctor.findOne({id: doctorId})
    const _patient = await Patient.findOne({id: patientId})

    _d.doctorId = _doctor._id
    _d.patientId = _patient._id
    // console.log(_d)
    const _appointment = new Appointment(_d);
    appointment = _appointment.save();

    return res.status(201).json({
      message: "Appointment schedule Successful",
      appointment,
    });
    // )
  } catch (error) {
    // if (error) {
    return res.status(409).json({
      error: true,
      message: "Appointment schedule Failed",
      err: error,
    });
    // }
  }
};

// Read

exports.find = async (req, res) => {
  try {
    const appointment = await Appointment.find({}).populate('doctorId patientId');
    // console.log(appointment);
    return res.status(200).json({ appointment });
  } catch (error) {
    console.log(error);
    return res.status(403).json({ error });
  }
};

// Update

exports.update = async (req, res) => {
  // console.log('here', req.body);
  // console.log(keys);
  // return res.json({});
  try {
    const user = await Appointment.findOneAndUpdate(
      { id: req.params.id },
      req.body,
      {
        new: true,
      }
    );
    return res.status(200).json({
      user,
    });
  } catch (e) {
    return res.status(400).json({
      message: "Server Error",
      e,
    });
  }
};
