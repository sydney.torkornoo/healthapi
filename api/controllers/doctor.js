const User = require("../models/Doctor");

// Create
exports.create = async (req, res, next) => {
  const { id, name, dob, gender, phoneNumber } = req.body;
  let _d = {
    id,
    name,
    dob,
    gender,
    phoneNumber,
  };
  try {
    // console.log(_d)
    // return res.status(401).json(_d)
    const _doctor = new User(_d);
    doctor = _doctor.save();

    return res.status(201).json({
      message: "Profile Setup Successful",
      doctor,
    });
    // )
  } catch (error) {
    // if (error) {
    return res.status(409).json({
      error: true,
      message: "Profile Setup Failed",
      err: error,
    });
    // }
  }
};


// Read

exports.find = async (req, res) => {
  try {
    const doctors = await User.find({});
    console.log(doctors);
    return res.status(200).json({ doctors });
  } catch (error) {
    console.log(error);
    return res.status(403).json({ error });
  }
};

// Update

exports.update = async (req, res) => {
  // console.log('here', req.body);
  // console.log(keys);
  // return res.json({});
  try {
    const user = await User.findOneAndUpdate(
      { id: req.params.id },
      req.body,
      { new: true }
    );
    return res.status(200).json({
      user,
    });
  } catch (e) {
    return res.status(400).json({
      message: "Server Error",
      e,
    });
  }
};