const Patient = require("../models/Patient");
const Appointment = require("../models/Appointment");
const Log = require("../models/Log");

// Create
exports.create = async (req, res, next) => {
  const { id, name, dob, gender, phoneNumber } = req.body;
  let _d = {
    id,
    name,
    dob,
    gender,
    phoneNumber,
  };
  try {
    const _patient = new Patient(_d);
    patient = _patient.save();

    return res.status(201).json({
      message: "Profile Setup Successful",
      patient,
    });
    // )
  } catch (error) {
    // if (error) {
    return res.status(409).json({
      error: true,
      message: "Profile Setup Failed",
      err: error,
    });
    // }
  }
};

// Read

exports.find = async (req, res) => {
  try {
    const patients = await Patient.find({});
    // console.log(patients);
    return res.status(200).json({ patients });
  } catch (error) {
    console.log(error);
    return res.status(403).json({ error });
  }
};

exports.profile = async (req, res) => {
  try {
    const patient = await Patient.findOne({id: req.params.id});
    // console.log({patient});
    ret = {patient}
    if(patient) {
      const appointments = await Appointment.find({patientId:patient._id}).populate('doctorId');
      const logs = await Log.find({patientId:patient._id}).populate('doctorId');
      ret.appointments = appointments
      ret.logs = logs
    }
    console.log({ret})
    return res.status(200).json(ret);
  } catch (error) {
    console.log(error);
    return res.status(403).json({ error });
  }
};

// Update

exports.update = async (req, res) => {
  // console.log('here', req.body);
  // console.log(keys);
  // return res.json({});
  try {
    const user = await Patient.findOneAndUpdate(
      { id: req.params.id },
      req.body,
      { new: true }
    );
    return res.status(200).json({
      user,
    });
  } catch (e) {
    return res.status(400).json({
      message: "Server Error",
      e,
    });
  }
};