const mongoose = require("mongoose");

const DoctorSchema = new mongoose.Schema({
  name: {
      type: String,
      required: true,
  },
  phoneNumber: {
    type: String,
    trim: true,
    required: true,
    unique: true,
  },
  id: {
    type: String,
  },
  dob: {
    type: String,
  },
  gender: {
    type: String,
  },
  status: {
    type: String,
    default: 'on-duty'
  },
  registeredOn: {
    type: Number,
    default: Date.now()
  },
});

module.exports = mongoose.model("Doctor", DoctorSchema);
