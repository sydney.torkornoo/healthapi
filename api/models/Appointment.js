const mongoose = require("mongoose");

const AppointmentSchema = new mongoose.Schema({
  doctorId: {
    type: String,
    required: true,
    ref: 'Doctor'
  },
  patientId: {
    type: String,
    required: true,
    ref: 'Patient'
  },
  date: {
    type: String,
    required: true,
  },
  time: {
    type: String,
    required: true,
  },
  duration: {
    type: String,
    required: true,
  },
  status: {
    type: String,
    default: "pending",
  },
  registeredOn: {
    type: Number,
    default: Date.now(),
  },
});

module.exports = mongoose.model("Appointment", AppointmentSchema);
