const mongoose = require("mongoose");

const UserSchema = new mongoose.Schema({
  isOnline: {
    type: Boolean,
    default: false
  },
  socket: {
    type: String
  },
  name: {
    first: {
      type: String,
      required: true,
    },
    last: {
      type: String,
      required: true,
    },
    display: {
      type: String,
    },
  },
  username: {
    type: String,
    // required: true,
    // unique: true,
  },
  code: {
    type: String,
  },
  email: {
    type: String,
    unique: false,
    required: false,
    default: ''
  },
  emailVerified: {
    type: Boolean,
    default: false,
  },
  phoneNumber: {
    type: String,
    trim: true,
    required: true,
    unique: true,
  },
  uid: {
    type: String,
    trim: true,
    required: true,
    unique: true,
  },
  country: {
    type: String,
  },
  region: {
    type: String,
  },
  city: {
    type: String,
  },
  dob: {
    type: Number,
  },
  gender: {
    type: String,
  },
  password: {
    type: String,
  },
  profile_picture: {
    type: String
  },
  access: {
    type: String,
    default: 'public'
  },
  status: {
    type: String,
    default: 'active'
  },
  registeredOn: {
    type: Number,
    default: Date.now()
  },
  premium: {
    type: Boolean,
    required: false,
    default: false,
  },
  subscription: {
    type: Object,
    required: false
  },
  packages: {
    type: Object,
    required: false
  }
});

module.exports = mongoose.model("User", UserSchema);
