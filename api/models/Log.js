const mongoose = require("mongoose");

const LogSchema = new mongoose.Schema({
  doctorId: {
    type: String,
    ref: 'Doctor',
    // required: true,
  },
  patientId: {
    type: String,
    // required: true,
  },
  date: {
    type: String,
    // required: true,
  },
  description: {
    type: String,
    // required: true,
  },
  time: {
    type: String,
    // required: true,
  },
  remarks: {
    type: String,
  },
  billable: {
    type: Boolean,
    default: false,
  },
  bill: {
    type: Number,
    default: 0,
  },
  paid: {
    type: Boolean,
    default: false,
  },
  completed: {
    type: Boolean,
    default: false,
  },
  createdOn: {
    type: Number,
    default: Date.now(),
  },
});

module.exports = mongoose.model("Log", LogSchema);
