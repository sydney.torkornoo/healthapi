const mongoose = require("mongoose");

const PatientSchema = new mongoose.Schema({
  id: {
      type: String,
      required: true,
  },
  name: {
      type: String,
      required: true,
  },
  phoneNumber: {
    type: String,
    trim: true,
    required: true,
    unique: true,
  },
  dob: {
    type: String,
  },
  gender: {
    type: String,
  },
  status: {
    type: String,
    default: 'checked-in'
  },
  registeredOn: {
    type: Number,
    default: Date.now()
  },
});

module.exports = mongoose.model("Patient", PatientSchema);
