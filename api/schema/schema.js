const {
  GraphQLObjectType,
  GraphQLID,
  GraphQLString,
  GraphQLBoolean,
  GraphQLSchema,
  GraphQLList,
} = require("graphql");

const {q} = require('../../psqlAdapter')

const FreeTipsType = new GraphQLObjectType({
  name: "FreeTips",
  fields: () => ({
    id: { type: GraphQLID },
    heading: { type: GraphQLString },
    url: { type: GraphQLString },
    hashtags: { type: GraphQLString },
    top: { type: GraphQLBoolean },
    image: { type: new GraphQLList(GraphQLString) },
  }),
});

const RootQuery = new GraphQLObjectType({
  name: "RootQueryType",
  fields: {
    freetips: {
      type: new GraphQLList(FreeTipsType),
      async resolve(parent, args) {
        const _q = q('SELECT * FROM freeTips')
        // console.log(_q)
        return _q
      },
    },
    freetip: {
      type: FreeTipsType,
      args: { id: { type: GraphQLID } },
      async resolve(parent, args) {
        const _q = await q(`SELECT * FROM freeTips WHERE id = ${args.id}`)
        // console.log(_q)
        return _q[0]
      },
    },
  },
});

module.exports = new GraphQLSchema({
  query: RootQuery,
});
