const jwt = require("jsonwebtoken");

module.exports.encryptedbody = (req, res, next) => {
  let code = parseInt(new Date().valueOf() / 10000);
  let phrase = "23CUR3 c0d3 70R 13t2r011.";
  let secret = phrase;
  const token = req.body.token.replace(/"/g, "");
  // // console.log(token);
  try {
    data = jwt.verify(token, secret);
    req.body = data;
    // // console.log(data)
    next();
  } catch (error) {
    // console.log("e", error);
    return res.status(403).json({
      message: "Request Failed",
      error,
    });
  }
};

module.exports.body = (req, res, next) => {
  // console.log(req.body);
  try {
    const token = req.body.token;
    // console.log(token);
    let data = JSON.parse(token);
    let new_data = { ...req.body, ...data, user: req.body.user };
    // // console.log(new_data);
    delete new_data.token;
    req.body = new_data;
  } catch (e) {
    let data = {};
    try {
      const token = req.body.token;
      // // console.log(token);
      data = JSON.parse(token);
    } catch (error) {
      data = {};
    }
    let new_data = { ...req.body, ...data };
    delete new_data.token;
    req.body = new_data;
  } finally {
    // console.log("finally", req.body);
    next();
  }
};
