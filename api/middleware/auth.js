const jwt = require("jsonwebtoken");

module.exports = (req, res, next) => {
  let token = req.headers.authorization;
  // console.log(token);
  if (token === undefined) {
    return res.status(403).json({
      message: "Access Denied. Invalid Validation token",
      error: true,
    });
  }

  token = token.replace("Bearer ", "");
  // console.log({token})
  try {
    let data = jwt.verify(token, '23CUR3 c0d3 70R 13t2r011.')
    if (req.baseUrl === "/user") {
      req.headers.user = data;
    }
    if (req.baseUrl === "/cetech") {
      req.headers.user = data;
    }
    // console.log(req.headers.user)
    next();
  } catch (err) {
    // console.log(err);
    return res.status(403).json({
      err: err,
      message: "Access Denied",
      error: true,
    });
  }
};
