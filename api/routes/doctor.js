const express = require("express");
const router = express.Router();

const { body, encryptedbody } = require("../middleware/getbody");
const { create, find, update } = require("../controllers/doctor");

router.post("/", body, create);

// Read
router.get("/", body, find);
// Read end

// Update
router.patch("/:id", body, update);
// Update end


module.exports = router;
