const express = require("express");
const router = express.Router();

const { body, encryptedbody } = require("../middleware/getbody");
const { create, find, update, profile } = require("../controllers/log");

// Create 
router.post("/", body, create);
// Create end

// Read
router.get("/", body, find);
router.get("/:id", body, profile);
// Read end

// Update
router.patch("/:id", body, update);
// Update end


module.exports = router;
