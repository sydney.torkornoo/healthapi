const graphql =  require('graphql'); 
const FreeTip = require('../../models/FreeTip');

const {
    GraphQLObjectType,
    GraphQLID,
    GraphQLString,
    GraphQLList,
    GraphQLSchema,
    GraphQLNonNull
} = graphql;

//DEFINE TYPES
const FreeTipType = new GraphQLObjectType({
    name: 'FreeTip',
    fields: () => ({
        // id: {type: GraphQLID},
        heading: {type: GraphQLString},
        url: {type: GraphQLString},
        hashtags: {type: GraphQLString},
        articleDetails: {type: GraphQLString},
        image: {type: new GraphQLList(GraphQLString)},
    })
})

const RootQuery = new GraphQLObjectType({
    name: 'RootQueryType',
    fields: {
        freeTips: {
            type: new GraphQLList(FreeTipType),
            resolve(parent, args) {
                console.log('Requesting data from tips...');
                return FreeTip.find({})
            }
        }
    }
})

module.exports = new GraphQLSchema({
    query: RootQuery
})