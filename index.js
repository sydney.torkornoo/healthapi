"use strict";
const util = require('util');
global.TextEncoder = util.TextEncoder;
global.TextDecoder = util.TextDecoder;
require("dotenv").config();
const express = require("express");
const mongoose = require("mongoose");
const morgan = require("morgan");
const cors = require("cors");

const app = express();
// const socket = require("socket.io");
let c = "not connected";

const db_conn = process.env.DB_CONN;
// console.log({db_conn})
mongoose.connect(db_conn);

app.use(
  cors({
    // origin: ['https://cp.accesspoint.biz', 'https://shop.cetechbpa.com', 'https://web.accesspoint.biz', 'http://localhost:3000', 'http://localhost:3001', 'http://localhost:5173', 'http://localhost:5799', 'http://127.0.0.1:5799', 'https://web-svelte-accesspoint.vercel.app', 'https://accesspoint.biz']
  })
);

app.use(morgan("dev"));
app.use(express.urlencoded({ extended: false }));
app.use(express.json());

const patient = require("./api/routes/patient");
const doctor = require("./api/routes/doctor");
const appointment = require("./api/routes/appointment");
const log = require("./api/routes/log");

app.use("/patient", patient);
app.use("/doctor", doctor);
app.use("/appointment", appointment);
app.use("/log", log);

app.use("*", (req, res) => {
  res.status(404).send("<h1>Page not Found</h1>" + c);
});

// const server =
app.listen(process.env.PORT || 3113, () =>
  console.log(`listening on port ${process.env.PORT || 3113}`)
);

// const io = socket(server, {
//   cors: {
//     credentials: true,
//   },
// });
// // console.log(io);
// app.set("socketio", io);
// global.onlineUsers = new Map();
// io.on("connection", (socket) => {
//   global.chatSocket = socket;

//   socket.on("add-user", (id) => {
//     onlineUsers.set(id, socket.id);
//     if (id.indexOf("+") === -1) {
//       User.findOneAndUpdate(
//         { _id: id },
//         { socket: socket.id, isOnline: true },
//         { new: true },
//         function (err, doc) {
//           // console.log(err);
//           // console.log(doc);
//           if (err) {
//             console.log("connection failed", err);
//           }
//         }
//       );
//     }
//   });

//   socket.on("disconnect", () => {
//     // console.log(socket.id);
//     // console.log(onlineUsers);
//     onlineUsers.forEach((key, value) => {
//       if (value === socket.id) {
//         onlineUsers.delete(key);
//       }
//     });
//     // console.log(onlineUsers);
//     User.findOneAndUpdate(
//       { socket: socket.id },
//       { socket: "", isOnline: false },
//       { new: true },
//       function (err, doc) {
//         // console.log(err);
//         // console.log(doc);
//         if (err) {
//           console.log("connection failed", err);
//         }
//         // console.log("connection successful", doc);
//       }
//     );
//   });

//   socket.on("send-message", (data) => {
//     console.log("send message", data);
//     const sendUSocket = onlineUsers.get(data.to);
//     if (sendUSocket) {
//       socket.to(sendUSocket).emit("message", data);
//     }
//   });

//   socket.on("send-provider", (data) => {
//     console.log("send to provider", data);
//     const sendUSocket = onlineUsers.get(data.to);
//     if (sendUSocket) {
//       socket.to(sendUSocket).emit("provider", data);
//     }
//   });

//   socket.on("message-sent", async (data) => {
//     // console.log("send message", data.to._id);
//     const sendUSocket = onlineUsers.get(data.to._id);
//     if (sendUSocket) {
//       socket.to(sendUSocket).emit("new-message", data);
//     } else {
//     }
//   });

//   socket.on("call-user", (data) => {
//     const sendUSocket = onlineUsers.get(data.to);
//     if (sendUSocket) {
//       socket.to(sendUSocket).emit("call-user", data);
//     }
//   });

//   socket.on("answer-call", (data) => {
//     const sendUSocket = onlineUsers.get(data.to);
//     if (sendUSocket) {
//       socket.to(sendUSocket).emit("call-accepted", data);
//     }
//   });

//   socket.on("end-call", (data) => {
//     const sendUSocket = onlineUsers.get(data.to);
//     if (sendUSocket) {
//       socket.to(sendUSocket).emit("call-ended", data);
//     }
//   });

//   // _p.once("change", async (change) => {
//   //   if (change.operationType === "insert") {
//   //     let doc = change.fullDocument;
//   //     // console.log(doc)
//   //     const _ret = await ProviderMessage.findOne({ _id: doc._id }).populate(
//   //       "from to product"
//   //     );

//   //     if (_ret.to.isOnline) {
//   //       // console.log(_ret.to.socket, onlineUsers)
//   //       console.log('data');
//   //       socket.to(_ret.to.socket).emit("provider", _ret);
//   //     }
//   //   }
//   // });
// });
